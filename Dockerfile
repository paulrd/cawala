FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/cw.jar /cw/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/cw/app.jar"]
