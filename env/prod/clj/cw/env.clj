(ns cw.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[cw started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[cw has shut down successfully]=-"))
   :middleware identity})
