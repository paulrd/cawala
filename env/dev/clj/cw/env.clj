(ns cw.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [cw.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[cw started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[cw has shut down successfully]=-"))
   :middleware wrap-dev})
