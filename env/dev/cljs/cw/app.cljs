(ns ^:figwheel-no-load cw.app
  (:require [cw.core :as core]
            [devtools.core :as devtools]))

(enable-console-print!)

(devtools/install!)

(core/init!)
