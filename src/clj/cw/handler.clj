(ns cw.handler
  (:require [compojure.core :refer [routes wrap-routes]]
            [cw.layout :refer [error-page]]
            [cw.routes.home :refer [home-routes]]
            [cw.routes.services :refer [service-routes]]
            [compojure.route :as route]
            [cw.env :refer [defaults]]
            [mount.core :as mount]
            [cw.middleware :as middleware]))

(mount/defstate init-app
  :start ((or (:init defaults) identity))
  :stop  ((or (:stop defaults) identity)))

(mount/defstate app
  :start
  (middleware/wrap-base
    (routes
      (-> #'home-routes
          (wrap-routes middleware/wrap-csrf)
          (wrap-routes middleware/wrap-formats))
          #'service-routes
      (route/not-found
        (:body
          (error-page {:status 404
                       :title "page not found"}))))))
